workbench_name = "autobench"

#iam user
aws_iam_user = "shivani.mamidwar@ezest.in"
account_id = "503990129501"
stage = "dev"



#sagemaker
sagemaker_enable = false
instance_name = "sagemaker-notebook"
instance_type = "ml.m5.xlarge"

#s3
s3_enable = false
bucket_name = "dse-test-bucket-demo-ds-user1"


#lambda
function_name="dspredictions21"


#attach policy
# attach_policy_enable = false

#services
athena_service_enable = false
glue_service_enable = false
