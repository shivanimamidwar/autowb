

module "lambda" {
  source = "../../modules/lambda"
  function_name = "ca-dse-${var.function_name}-wb-${var.workbench_name}-fn"
}

module "sagemaker" {
  source = "../../modules/sagemaker"
  instance_type = var.instance_type
  # instance_name = "ca-dse-${var.instance_name}-wb-${var.workbench_name}-noteb"
  athena_service_enable = "ca-dse-${var.stage}-wb-${var.workbench_name}-iam-at"
  glue_service_enable = "ca-dse-${var.stage}-wb-${var.workbench_name}-iam-gs"

  bucket_name = "ca-dse-${var.bucket_name}-wb-${var.workbench_name}-gen-p-b"
  # bucket_caspian = "ca-dse-${var.bucket_caspian}-wb-${var.workbench_name}-casp-b"
  # caspian_side_bucket = var.caspian_side_bucket
  general_purpose_bucket_arn = var.general_purpose_bucket_arn 
  s3_enable = "ca-dse-${var.stage}-wb-${var.workbench_name}-iam-s3"
  aws_iam_user = var.aws_iam_user 
  
}

  
  



  
  


  
  
  




  



  
  



