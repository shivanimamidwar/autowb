
data "archive_file" "sagemaker_lambda" {
  type        = "zip"
  source_file  = "${"../../modules/lambda/sagemaker_lambda.py"}"
  output_path  = "${"../../modules/lambda/sagemaker_lambda.zip"}"
}



resource "aws_lambda_function" "lambda_function" {
  
  function_name = var.function_name
  filename = "${"../../modules/lambda/sagemaker_lambda.zip"}"
  role = aws_iam_role.lambda_role.arn
  handler = "exports.test"
  runtime = "python3.9"
    
}

resource "aws_iam_role" "lambda_role" {
  name = "lambda_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy" "lambda_policy" {
  name = "lambda_policy"
  role = aws_iam_role.lambda_role.id
  
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
    {
      "Sid": "Stmt1672904312992",
      "Action": "sagemaker:*",
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
  })
}





