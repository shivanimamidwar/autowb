resource "aws_iam_policy" "s3_policy" {
  name        = "dse-s3-bucket-policy"
  path        = "/"
  description = "S3 test policy"
  count       = "${var.s3_enable == true ? 1 : 0}"	
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:ListAllMyBuckets",
            "Resource": "arn:aws:s3:::*"
        },
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::dse-test-bucket-demo",
                "arn:aws:s3:::dse-test-bucket-demo/*"
            ]
        }
    ]
        }
  EOF
}

#Attach policy to user

resource "aws_iam_policy_attachment" "s3_policy_attach" {
  name       = "dse-s3-bucket-policy-attachment"
  count      = "${var.s3_enable == true ? 1 : 0}"
  users      = ["${var.aws_iam_user}"]        
  policy_arn = "${aws_iam_policy.s3_policy[0].arn}"
}


