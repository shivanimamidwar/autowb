
# Creation of sagemaker-role
resource "aws_iam_role" "sagemaker-ni-role" {
  name = "sagemaker-ni-role"
  count 	= "1"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "sagemaker.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}
#attach policy to user role
resource "aws_iam_role_policy" "user_policy" {
  count 	= var.sagemaker_enable == true ? 1 : 0
  role = aws_iam_role.sagemaker-ni-role[count.index].id
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
      "Sid": "SagemakerNiStopStartDescUrl",
      "Effect": "Allow",
        "Action": [
          "sagemaker:StopNotebookInstance",
          "sagemaker:StartNotebookInstance",
          "sagemaker:CreatePresignedNotebookInstanceUrl"
        ],
      "Resource": "${aws_sagemaker_notebook_instance.sagemaker-ni.arn}"
    },
    {
      "Sid": "SagemakerNiListAllInstRepo",
      "Effect": "Allow",
        "Action": [
            "sagemaker:List*",
            "sagemaker:Describe*",
            "sagemaker:AddTags",
            "sagemaker:AssociateTrialComponent",
            "sagemaker:BatchGetMetrics",
            "sagemaker:BatchPutMetrics",
            "sagemaker:CreateAlgorithm",
            "sagemaker:CreateApp",
            "sagemaker:CreateAutoMLJob",
            "sagemaker:CreateCompilationJob",
            "sagemaker:CreateExperiment",
            "sagemaker:CreateFlowDefinition",
            "sagemaker:CreateHumanTaskUi",
            "sagemaker:CreateHyperParameterTuningJob",
            "sagemaker:CreateLabelingJob",
            "sagemaker:CreateModel",
            "sagemaker:CreateModelPackage",
            "sagemaker:CreateMonitoringSchedule",
            "sagemaker:CreateNotebookInstanceLifecycleConfig",
            "sagemaker:CreatePresignedDomainUrl",
            "sagemaker:CreatePresignedNotebookInstanceUrl",
            "sagemaker:CreateProcessingJob",
            "sagemaker:CreateTrainingJob",
            "sagemaker:CreateTransformJob",
            "sagemaker:CreateTrial",
            "sagemaker:CreateTrialComponent",
            "sagemaker:DeleteAlgorithm",
            "sagemaker:DeleteApp",
            "sagemaker:DeleteExperiment",
            "sagemaker:DeleteFlowDefinition",
            "sagemaker:DeleteHumanLoop",
            "sagemaker:DeleteModel",
            "sagemaker:DeleteModelPackage",
            "sagemaker:DeleteMonitoringSchedule",
            "sagemaker:DeleteNotebookInstanceLifecycleConfig",
            "sagemaker:DeleteTags",
            "sagemaker:DeleteTrial",
            "sagemaker:DeleteTrialComponent",
            "sagemaker:DisassociateTrialComponent",
            "sagemaker:GetSearchSuggestions",
            "sagemaker:InvokeEndpoint",
            "sagemaker:RenderUiTemplate",
            "sagemaker:Search",
            "sagemaker:StartHumanLoop",
            "sagemaker:StartMonitoringSchedule",
            "sagemaker:StopAutoMLJob",
            "sagemaker:StopCompilationJob",
            "sagemaker:StopHumanLoop",
            "sagemaker:StopHyperParameterTuningJob",
            "sagemaker:StopLabelingJob",
            "sagemaker:StopMonitoringSchedule",
            "sagemaker:StopProcessingJob",
            "sagemaker:StopTrainingJob",
            "sagemaker:StopTransformJob",
            "sagemaker:UpdateCodeRepository",
            "sagemaker:UpdateEndpoint",
            "sagemaker:UpdateEndpointWeightsAndCapacities",
            "sagemaker:UpdateExperiment",
            "sagemaker:UpdateMonitoringSchedule",
            "sagemaker:UpdateNotebookInstanceLifecycleConfig",
            "sagemaker:UpdateTrial",
            "sagemaker:UpdateTrialComponent"
        ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListAllMyBuckets",
        "s3:GetBucketLocation"
      ],
      "Resource": "arn:aws:s3:::*"
    },
    {
      "Effect": "Allow",
      "Action": "s3:ListBucket",
      "Resource": "${aws_s3_bucket.gp[count.index].arn}",
      "Condition": {
        "StringEquals" : 
          {
            "s3:prefix":["","Athena/"],"s3:delimiter":"/"
          }
        }
    },
    {
      "Effect": "Allow",
      "Action": "s3:ListBucket",
      "Resource": "${aws_s3_bucket.gp[count.index].arn}",
      "Condition": {
        "StringLike" : 
          {
            "s3:prefix":"Athena/*"
          }
      }
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject"
      ],
      "Resource": "${aws_s3_bucket.gp[count.index].arn}/Athena/*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetObject"
      ],
      "Resource": "${aws_s3_bucket.gp[count.index].arn}/*"
    },
    {
      "Action": [
        "iam:PassRole"
      ],
      "Effect": "Allow",
      "Resource": "${aws_iam_role.glue_role.arn}",
      "Condition": {
        "StringLike": {
          "iam:PassedToService": [
            "glue.amazonaws.com"
          ]
        }
      }
    },
    {
      "Sid": "ListRolesToPerformPassRole",
      "Effect": "Allow",
        "Action": [
          "iam:ListRoles",
          "iam:GetRole"
        ],
      "Resource": "*"
    }
  ]
}
EOF
}